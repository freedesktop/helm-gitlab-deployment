certmanager:
  install: false

nginx-ingress:
  enabled: false
gitlab:
  gitlab-grafana:
    ingress:
      annotations:
        nginx.ingress.kubernetes.io/permanent-redirect: https://grafana.freedesktop.org
  migrations:
    enabled: true
  sidekiq:
    enabled: true
    pods:
    - name: ci-destroy-old-pipelines
      queues: ci-destroy-old-pipelines
      minReplicas: 1
      maxReplicas: 1
    - name: default
      queues: default,mailers
  toolbox:
    enabled: true
    backups:
      objectStorage:
        backend: 's3'
        config:
          secret: 'freedesktop-prod-ceph-backups-key'
          key: 'config'
      cron:
         enabled: false
         schedule: 00 04 * * *
         extraArgs: --skip artifacts --skip registry --skip lfs --skip uploads --skip pages
         persistence:
           enabled: true
           storageClass: local-path
           size: 4Ti
    # we need the local-path storage, so we need to schedule
    # toolbox pods on nodes with enough disk
    nodeSelector:
      fdo.plan: s3.xlarge.x86
    persistence:
      enabled: true
      size: '512Gi'
      storageClass: local-path
  gitaly:
    persistence:
      size: "520Gi"
      storageClass: rook-ceph-block-ssd
    securityContext:
      # The following intends to speed up the gitaly containers
      # spin up time. It only works for existing volumes, so
      # this needs to be removed when bootstrapping a gitlab
      # instance.
      # See https://gitlab.com/gitlab-org/charts/gitlab/-/merge_requests/1665
      # for the technical discussion
      fsGroup: ""
    extraVolumeMounts: |
      - mountPath: /gitlab-ssh-keys
        name: sshkeys
        readOnly: true
    extraVolumes: |
      - name: sshkeys
        secret:
          defaultMode: 420
          items:
          - key: kemper_ssh_priv_key
            path: kemper-ssh-priv
          - key: kemper_ssh_known_host
            path: kemper-ssh-known-hosts
          - key: git_post_receive_mirror_hook
            mode: 365
            path: git-post-receive-mirror
          secretName: freedesktop-prod-post-receive-hook-secret
    tolerations:
      - key: "fdo.plan"
        operator: "Equal"
        value: "s3.xlarge.x86"
        effect: "NoSchedule"
  gitlab-pages:
    service:
      customDomains:
        type: ClusterIP
    ingress:
      tls:
        secretName: gitlab-prod-pages-tls
  webservice:
    extraEnv:
      GITLAB_RAILS_RACK_TIMEOUT: "600"
      GITLAB_WORKHORSE_SENTRY_DSN: http://{{readFile "../../helm-gitlab-secrets/secrets.yaml" | fromYaml | get "global.sentry.pubkey"}}@sentry.sentry.svc/2
      GITLAB_WORKHORSE_SENTRY_ENVIRONMENT: production
    minReplicas: 7
    maxReplicas: 7

postgresql:
  install: false

prometheus:
  server:
    persistentVolume:
      storageClass: rook-ceph-block-ssd
      size: 20Gi

redis:
  master:
    persistence:
      enabled: false
      medium: Memory
      sizeLimit: "10Gi"

registry:
  enabled: true
  storage:
    key: 'config'
    secret: freedesktop-prod-ceph-registry-key
    redirect:
      disable: true
  ingress:
    tls:
      secretName: gitlab-prod-registry-tls
  maintenance:
    readOnly:
      enabled: false
  tokenIssuer: 'omnibus-gitlab-issuer'
  log:
    level: info
  hpa:
    minReplicas: 4
    maxReplicas: 4
  database:
    enabled: true
    host: postgresql-registry-prod
    # host: postgresql-registry-prod.gitlab.svc.dc.clusterset.k3s
  migration:
    enabled: false
  gc:
    disabled: false
